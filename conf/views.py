from django.shortcuts import render, redirect
from django.views.generic import CreateView

from .models import Speaker
from .forms import SpeakerForm


def index(request):
    speakers = Speaker.objects.all()
    return render(request, 'conf/index.html', {'speakers': speakers})


def add_speaker(request):
    if request.method == 'POST':
        form = SpeakerForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('index')
    else:
        form = SpeakerForm()
    return render(request, 'conf/add_speaker.html', {'form': form})


class AddSpeaker(CreateView):
    template_name = 'conf/add_speaker.html'
    form_class = SpeakerForm
